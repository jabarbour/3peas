import { Component } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { BackandService } from "@backand/angular2-sdk";
import { MessageService } from "../../core/services/message.service";

@Component({
    selector   : 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls  : ['./toolbar.component.scss']
})

export class FuseToolbarComponent
{
    showSpinner: boolean;
    user: any = {};

    constructor(private router: Router,
                private message: MessageService,
                private backand: BackandService)
    {
        this.user = JSON.parse(localStorage.getItem('userData'));

        router.events.subscribe(
            (event) => {
                if ( event instanceof NavigationStart )
                {
                    this.showSpinner = true;
                }
                if ( event instanceof NavigationEnd )
                {
                    this.showSpinner = false;
                }
            });
    }


    /**
     * Logout a user from account
     */
    logout(){
        this.backand.signout()
            .then(
                (success) => {
                    localStorage.clear();

                    //show a message
                    this.message.show('Success', "You are logged out from your account.", "blue-500-fg");

                    //close a message
                    setTimeout(() => {
                        this.message.close();
                    }, 1200);

                    setTimeout(() => {
                        this.router.navigate(['login']);
                    }, 1500);
                }
            )
            .catch(
                (error) => {
                    let errorMessage = error.data.error_description;

                    //show a message
                    this.message.show('Error', errorMessage, "warn-500-fg");
                });
    }
}
