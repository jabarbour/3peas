import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { Page } from "../page.model";
import { ProductsService } from "../../../../core/services/products/products.service";

@Injectable()
export class ProductsListService implements Resolve<any> {
    onProductsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onSearchTextChanged: Subject<any> = new Subject();
    onPageChanged: Subject<any> = new Subject();
    products = [];
    searchText: string = "";
    page = new Page();

    constructor(private productsService: ProductsService) {
    }

    /**
     * The Products App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([])
                .then(
                    () => {

                        this.onPageChanged.subscribe(page => {
                            this.page = page;
                            this.getProducts(this.page, this.searchText);
                        });

                        this.onSearchTextChanged.subscribe(searchText => {
                            this.searchText = searchText;
                            this.getProducts(this.page, searchText);
                        });

                        resolve();
                    },
                    reject
                );
        });
    }

    /**
     * Populate the table with new data based on the page number
     *
     * @param page
     * @param filter
     * @returns {Promise}
     */
    getProducts(page, filter?): Promise<any> {
        return new Promise((resolve, reject) => {
            page.pageNumber = typeof filter == 'undefined' || filter == "" ? page.pageNumber : 0;

            this.productsService.getListWithPagination(filter, page.size, page.pageNumber)
                .then((result) => {
                    this.products = result['data'];
                    this.onProductsChanged.next(this.products);
                    resolve(this.products);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    /**
     * Update the list of Products
     */
    updateProduct() {
        this.getProducts(this.page, this.searchText);
    }
}
