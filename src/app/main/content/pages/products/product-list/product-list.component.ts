import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductsListService } from '../products-list.service';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { MdDialog } from "@angular/material";
import { ProductFormDialogComponent } from "../product-form/product-form.component";
import { FormGroup } from "@angular/forms";
import { Page } from "../../page.model";

@Component({
    selector: 'product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

    size: number = 100;
    page = new Page(this.size);
    products = [];
    dialogRef: any;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private productsListService: ProductsListService,
                public dialog: MdDialog) {
        this.productsListService.onProductsChanged.subscribe(products => {
            this.products = products;

            if (this.products.length > 0) {
                this.page.totalElements = this.products[0].totalRows;
                this.page.totalPages = this.page.totalElements / this.page.size;
            }else{
               this.page = new Page(this.size);
            }
        });

    }

    ngOnInit() {
        //update the object Page
        this.productsListService.onPageChanged.next(this.page);
    }

    /**
     * Populate the table with new data based on the page number
     *
     * @param page The page to select
     */
    setPage(page) {
        this.page.pageNumber = page.offset;

        //update the object Page
        this.productsListService.onPageChanged.next(this.page);
    }


    /**
     * Create a new Product
     */
    newProduct() {
        this.dialogRef = this.dialog.open(ProductFormDialogComponent, {
            disableClose: true,
            panelClass: 'product-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {

                if (!response) {
                    return;
                }

                this.productsListService.updateProduct();
            });
    }
}
