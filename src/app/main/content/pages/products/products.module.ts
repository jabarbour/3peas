import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FileService } from "../../../../core/services/file/file.service";
import { UnitsService } from "../../../../core/services/units/units.service";
import { ProductsService } from "../../../../core/services/products/products.service";
import { ProductsComponent } from "./products.component";
import { ProductsListService } from "./products-list.service";
import { ProductListComponent } from "./product-list/product-list.component";
import { ProductFormDialogComponent } from "./product-form/product-form.component";
import { CategoriesService } from "../../../../core/services/categories/categories.service";
import { IngredientsService } from "../../../../core/services/ingredients/ingredients.service";

const routes = [
    {
        path     : '**',
        component: ProductsComponent,
        resolve  : {
            products: ProductsListService
        }
    }
];

@NgModule({
    //every component must be declared in some NgModule
    //every component can belong to only one NgModule
    declarations: [
        ProductsComponent,
        ProductListComponent,
        ProductFormDialogComponent,
    ],
    // import other modules as dependencies
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    //provider is to be injected,
    // create and maintain a singleton instance of that class and pass it in the injection
    providers   : [
        ProductsListService,
        FileService,
        CategoriesService,
        UnitsService,
        ProductsService,
        IngredientsService
    ],
    //it registers components for offline compilation
    // so that they can be used with ViewContainerRef.createComponent()
    entryComponents: [ProductFormDialogComponent]
})

export class ProductsModule
{
}
