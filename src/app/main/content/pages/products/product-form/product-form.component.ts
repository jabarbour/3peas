import { Component, Inject, OnInit, ViewEncapsulation, ViewChild, Input } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import 'rxjs/Rx';
import { ProductsService } from "../../../../../core/services/products/products.service";
import { Product } from "./product/product.model";
import { UnitsService } from "../../../../../core/services/units/units.service";
import { IngredientsService } from "../../../../../core/services/ingredients/ingredients.service";
import { Observable } from "rxjs";


@Component({
    selector: 'product-form-dialog',
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.scss'],
    //how the template and the styles should be encapsulated
    encapsulation: ViewEncapsulation.None
})

export class ProductFormDialogComponent implements OnInit {
    @ViewChild('fileInput') fileInput;
    event: CalendarEvent;
    dialogTitle: string;
    productForm: FormGroup;
    action: string;
    product: any = {};
    nutrition = {};
    ingredients = [];
    units = {
        us: [],
        eu: []
    };
    filteredIngredients: Observable<string[]>;
    searchIngredient: FormControl = new FormControl('');

    constructor(public dialogRef: MdDialogRef<ProductFormDialogComponent>,
                private unitsService: UnitsService,
                public dialog: MdDialog,
                private productsService: ProductsService,
                @Inject(MD_DIALOG_DATA) private data: any,
                private ingredientsService: IngredientsService,
                private formBuilder: FormBuilder) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Product';

            if (data.product.id) {
                this.fillProductModel(data.product);
            }
        }
        else {
            this.dialogTitle = 'New Product';
            this.product = new Product();
            this.initProductForm();
        }
    }

    ngOnInit() {
        //Get the full list
        this.getListIngredients();

        this.units = this.unitsService.getListUnits();
        this.units['us'] = this.units['us'].filter(unit => unit.is_main == 1);

        this.filteredIngredients = this.searchIngredient
            .valueChanges
            .startWith(null)
            .map(val => val ? this.filter(val) : this.ingredients.slice());

        let dataForm = this.productForm.value;
        if(this.ingredients.length > 0) {
            let ingredient = this.ingredients.filter(ingredient => ingredient.id == dataForm.ingredientId)[0];

            if(ingredient)
                this.searchIngredient = new FormControl(ingredient['name_singular']);
        }
    }

    /**
     * Search for Ingredients
     *
     * @param val
     * @returns {any[]}
     */
    filter(val: string): string[] {
        return this.ingredients.filter(ingredient =>
        ingredient.name_singular.toLowerCase().indexOf(val.toLowerCase()) === 0);
    }

    /**
     * Set a value of IngredientId
     *
     * @param event
     * @param ingredientId
     */
    onSelectIngredient(event, ingredientId){
        if(event.isUserInput) {
            this.productForm.controls['ingredientProduct'].setValue(ingredientId);
        }
    }

    /**
     * Clear a value of ingredientProduct
     */
    clearValueIngredientProduct(){
        this.productForm.controls['ingredientProduct'].setValue('');
    }


    /**
     * Fill in the model Product
     *
     * @param product
     */
    fillProductModel(product) {
        this.product = new Product(product);
    }


    /**
     * Initial the form Product
     */
    initProductForm() {

        let newForm = this.formBuilder.group({
            id: [this.product['id'] || ''],
            name: [this.product['name'] || ''],
            unitProduct: [this.product['unitProduct'] || ''],
            ingredientProduct: [this.product['ingredientProduct'] || ''],
            price: [this.product['price'] || ''],
            size: [this.product['size'] || ''],
            is_organic: [this.product['is_organic'] || ''],
            store: [this.product['store'] || '']
        });

        this.productForm = newForm;
    }


    /**
     * Save a new product to the Backand
     */
    createProduct() {
        let dataForm = this.productForm.value;

        console.log(dataForm);

        this.productsService.create(dataForm)
            .then(
                (result) => {
                    this.dialogRef.close('save');
                })
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * Get the list of all ingredients
     */
    getListIngredients() {
        this.ingredientsService.getList()
            .then(
                (result) => {
                    this.ingredients = result['data'];
                }
            )
    }
}