/**
 * An object used to get product information from the Backand
 */
export class Product {

    //The main info about the Product
    id: number;
    name: string = '';
    ingredientProduct: number;
    unitProduct: number;
    price: number;
    size: number;
    is_organic: boolean;
    store: string;

    constructor(product?:any){
        if(product){
            this.id = product['id'];
            this.name = product['name'];
            this.unitProduct = product['unitProduct'];
            this.ingredientProduct = product['ingredientProduct'];
            this.price = product['price'];
            this.size = product['size'];
            this.is_organic = product['is_organic'];
            this.store = product['store'];
        }
    }

}
