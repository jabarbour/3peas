import { Component, ViewEncapsulation } from '@angular/core';
import { FormControl } from "@angular/forms";
import { ProductsListService } from "./products-list.service";
import { Animations } from "../../../../core/animations";

@Component({
    selector   : 'products',
    templateUrl: './products.component.html',
    styleUrls  : ['./products.component.scss'],
    //how the template and the styles should be encapsulated
    encapsulation: ViewEncapsulation.None, //to use global CSS without any encapsulation
    animations   : [Animations.slideInTop]
})
export class ProductsComponent
{
    searchInput: FormControl;

    constructor(private productsListService: ProductsListService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {
        //Listener when a user starts to search in the list of products
        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.productsListService.onSearchTextChanged.next(searchText);
            });
    }
}
