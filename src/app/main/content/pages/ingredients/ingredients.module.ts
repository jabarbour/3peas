import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FileService } from "../../../../core/services/file/file.service";
import { UnitsService } from "../../../../core/services/units/units.service";
import { IngredientsService } from "../../../../core/services/ingredients/ingredients.service";
import { IngredientsComponent } from "./ingredients.component";
import { IngredientsListService } from "./ingredients-list.service";
import { IngredientListComponent } from "./ingredient-list/ingredient-list.component";
import { IngredientFormDialogComponent } from "./ingredient-form/ingredient-form.component";
import { CategoriesService } from "../../../../core/services/categories/categories.service";

const routes = [
    {
        path     : '**',
        component: IngredientsComponent,
        resolve  : {
            ingredients: IngredientsListService
        }
    }
];

@NgModule({
    //every component must be declared in some NgModule
    //every component can belong to only one NgModule
    declarations: [
        IngredientsComponent,
        IngredientListComponent,
        IngredientFormDialogComponent,
    ],
    // import other modules as dependencies
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    //provider is to be injected,
    // create and maintain a singleton instance of that class and pass it in the injection
    providers   : [
        IngredientsListService,
        FileService,
        CategoriesService,
        UnitsService,
        IngredientsService
    ],
    //it registers components for offline compilation
    // so that they can be used with ViewContainerRef.createComponent()
    entryComponents: [IngredientFormDialogComponent]
})

export class IngredientsModule
{
}
