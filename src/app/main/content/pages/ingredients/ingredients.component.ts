import { Component, ViewEncapsulation } from '@angular/core';
import { FormControl } from "@angular/forms";
import { IngredientsListService } from "./ingredients-list.service";
import { Animations } from "../../../../core/animations";

@Component({
    selector   : 'ingredients',
    templateUrl: './ingredients.component.html',
    styleUrls  : ['./ingredients.component.scss'],
    //how the template and the styles should be encapsulated
    encapsulation: ViewEncapsulation.None, //to use global CSS without any encapsulation
    animations   : [Animations.slideInTop]
})
export class IngredientsComponent
{
    searchInput: FormControl;

    constructor(private ingredientsListService: IngredientsListService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {
        //Listener when a user starts to search in the list of ingredients
        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.ingredientsListService.onSearchTextChanged.next(searchText);
            });
    }
}
