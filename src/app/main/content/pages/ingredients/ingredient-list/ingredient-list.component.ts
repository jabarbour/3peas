import { Component, OnInit, ViewChild } from '@angular/core';
import { IngredientsListService } from '../ingredients-list.service';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { MdDialog } from "@angular/material";
import { IngredientFormDialogComponent } from "../ingredient-form/ingredient-form.component";
import { FormGroup } from "@angular/forms";
import { Page } from "../../page.model";

@Component({
    selector: 'ingredient-list',
    templateUrl: './ingredient-list.component.html',
    styleUrls: ['./ingredient-list.component.scss']
})
export class IngredientListComponent implements OnInit {

    size: number = 100;
    page = new Page(this.size);
    ingredients = [];
    dialogRef: any;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private ingredientsListService: IngredientsListService,
                public dialog: MdDialog) {
        this.ingredientsListService.onIngredientsChanged.subscribe(ingredients => {
            this.ingredients = ingredients;

            if (this.ingredients.length > 0) {
                this.page.totalElements = this.ingredients[0].totalRows;
                this.page.totalPages = this.page.totalElements / this.page.size;
            }else{
                this.page = new Page(this.size);
            }
        });
    }

    ngOnInit() {
        //update the object Page
        this.ingredientsListService.onPageChanged.next(this.page);
    }

    /**
     * Populate the table with new data based on the page number
     *
     * @param page The page to select
     */
    setPage(page) {
        this.page.pageNumber = page.offset;

        //update the object Page
        this.ingredientsListService.onPageChanged.next(this.page);
    }


    /**
     * Create a new Ingredient
     */
    newIngredient() {
        this.dialogRef = this.dialog.open(IngredientFormDialogComponent, {
            disableClose: true,
            panelClass: 'ingredient-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {

                if (!response) {
                    return;
                }

                this.ingredientsListService.updateIngredient();
            });
    }
}
