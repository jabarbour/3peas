import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { BackandService } from "@backand/angular2-sdk";
import { FileService } from "../../../../core/services/file/file.service";
import { Page } from "../page.model";
import { IngredientsService } from "../../../../core/services/ingredients/ingredients.service";

@Injectable()
export class IngredientsListService implements Resolve<any> {
    onIngredientsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onSearchTextChanged: Subject<any> = new Subject();
    onPageChanged: Subject<any> = new Subject();
    ingredients = [];
    searchText: string = "";
    page = new Page();

    constructor(private ingredientsService: IngredientsService) {
    }

    /**
     * The Ingredients App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([])
                .then(
                    () => {

                        this.onPageChanged.subscribe(page => {

                            this.page = page;
                            this.getIngredients(this.page, this.searchText);
                        });

                        this.onSearchTextChanged.subscribe(searchText => {
                            this.searchText = searchText;
                            this.getIngredients(this.page, searchText);
                        });

                        resolve();
                    },
                    reject
                );
        });
    }

    /**
     * Populate the table with new data based on the page number
     *
     * @param page
     * @param filter
     * @returns {Promise}
     */
    getIngredients(page, filter?): Promise<any> {
        return new Promise((resolve, reject) => {
            page.pageNumber = typeof filter == 'undefined' || filter == "" ? page.pageNumber : 0;

            this.ingredientsService.getListWithPagination(filter, page.size, page.pageNumber)
                .then((result) => {
                    this.ingredients = result['data'];
                    this.onIngredientsChanged.next(this.ingredients);
                    resolve(this.ingredients);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }



    /**
     * Update the list of Ingredients
     */
    updateIngredient() {
        this.getIngredients(this.page, this.searchText);
    }
}
