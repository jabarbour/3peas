import { Component, Inject, OnInit, ViewEncapsulation, ViewChild, Input } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/Rx';
import { FileService } from "../../../../../core/services/file/file.service";
import { IngredientsService } from "../../../../../core/services/ingredients/ingredients.service";
import { Ingredient } from "./ingredient/ingredient.model";
import { UnitsService } from "../../../../../core/services/units/units.service";
import { CategoriesService } from "../../../../../core/services/categories/categories.service";


@Component({
    selector: 'ingredient-form-dialog',
    templateUrl: './ingredient-form.component.html',
    styleUrls: ['./ingredient-form.component.scss'],
    //how the template and the styles should be encapsulated
    encapsulation: ViewEncapsulation.None
})

export class IngredientFormDialogComponent implements OnInit {
    @ViewChild('fileInput') fileInput;
    event: CalendarEvent;
    dialogTitle: string;
    ingredientForm: FormGroup;
    action: string;
    ingredient: any = {};
    nutrition = {};
    uploadFile: any = {};
    categories = [];
    units = {
        us: [],
        eu: []
    };

    constructor(public dialogRef: MdDialogRef<IngredientFormDialogComponent>,
                private unitsService: UnitsService,
                public dialog: MdDialog,
                private ingredientsService: IngredientsService,
                @Inject(MD_DIALOG_DATA) private data: any,
                private categoriesService: CategoriesService,
                private fileService: FileService,
                private formBuilder: FormBuilder) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Ingredient';

            if (data.ingredient.id) {
                this.fillIngredientModel(data.ingredient);
            }
        }
        else {
            this.dialogTitle = 'New Ingredient';
            this.ingredient = new Ingredient();
            this.initIngredientForm();
        }
    }

    ngOnInit() {
        //Get the full list
        this.categories = this.categoriesService.getList();

        this.units = this.unitsService.getListUnits();
        this.units['us'] = this.units['us'].filter(unit => unit.is_main == 1);
    }


    /**
     * Fill in the model Ingredient
     *
     * @param ingredient
     */
    fillIngredientModel(ingredient) {
        this.ingredient = new Ingredient(ingredient);
    }


    /**
     * Initial the form Ingredient
     */
    initIngredientForm() {

        let newForm = this.formBuilder.group({
            id: [this.ingredient['id'] || ''],
            name_singular: [this.ingredient['name_singular'] || ''],
            name_plural: [this.ingredient['name_plural'] || ''],
            photo: [this.ingredient['photo'] || ''],
            unitIngredient: [this.ingredient['unitIngredient'] || ''],
            categoryIngredient: [this.ingredient['categoryIngredient'] || ''],
            calories: [this.ingredient['calories'] || ''],
            total_carbohydrates: [this.ingredient['total_carbohydrates'] || ''],
            sugar: [this.ingredient['sugar'] || ''],
            fat_calories: [this.ingredient['fat_calories'] || ''],
            total_fat: [this.ingredient['total_fat'] || ''],
            saturated_fat: [this.ingredient['saturated_fat'] || ''],
            trans_fat: [this.ingredient['trans_fat'] || ''],
            cholesterol: [this.ingredient['cholesterol'] || ''],
            sodium: [this.ingredient['sodium'] || ''],
            total_carbs: [this.ingredient['total_carbs'] || ''],
            dietary_fiber: [this.ingredient['dietary_fiber'] || ''],
            total_sugar: [this.ingredient['total_sugar'] || ''],
            added_sugar: [this.ingredient['added_sugar'] || ''],
            protein: [this.ingredient['protein'] || ''],
            potassium: [this.ingredient['potassium'] || ''],
            vitamin_a: [this.ingredient['vitamin_a'] || ''],
            vitamin_c: [this.ingredient['vitamin_c'] || ''],
            calcium: [this.ingredient['calcium'] || ''],
            iron: [this.ingredient['iron'] || '']
        });

        this.ingredientForm = newForm;
    }


    /**
     * Change a file
     *
     * @param $event
     */
    onFileChange($event) {
        if ($event.target.files.length > 0) {
            let file = $event.target.files[0];
            let myReader: FileReader = new FileReader();

            myReader.onloadend = (e) => {
                this.ingredientForm.controls['photo'].setValue(file);
                this.uploadFile = {
                    fileName: file.name,
                    base64: myReader.result
                };
            };
            myReader.readAsDataURL(file);
        } else {
            this.ingredientForm.controls['photo'].setValue('');
            this.uploadFile = {};
        }
    }


    /**
     * Save a new ingredient to the Backand
     */
    createIngredient() {

        let dataForm = this.ingredientForm.value;
        let fileName = this.uploadFile.fileName.replace(/ /gi, '_');

        //Upload a new image
        this.fileService.upload("ingredients", fileName, this.uploadFile.base64)
            .then(
                (fileUrl) => {
                    if (fileUrl) {
                        dataForm['photo'] = fileUrl;

                        this.ingredientsService.create(dataForm)
                            .then(
                                (result) => {
                                    this.dialogRef.close('save');
                                })
                            .catch(error => {
                                console.log(error);
                            });
                    }
                }
            )
            .catch(error => {
                console.log(error);
            });
    }
}