/**
 * An object used to get ingredient information from the Backand
 */
export class Ingredient {

    //The main info about the Ingredient
    id: number;
    name_singular: string = '';
    name_plural: string = '';
    photo: string = '';
    unitIngredient: number;
    categoryIngredient: number;
    calories: number;
    total_carbohydrates: number;
    sugar: number;
    total_fat: number;
    fat_calories: number;
    saturated_fat: number;
    trans_fat: number;
    cholesterol: number;
    sodium: number;
    total_carbs: number;
    dietary_fiber: number;
    total_sugar: number;
    added_sugar: number;
    protein: number;
    potassium: number;
    vitamin_a: number;
    vitamin_c: number;
    calcium: number;
    iron: number;

    constructor(ingredient?:any){
        if(ingredient){
            this.id = ingredient['id'];
            this.name_singular = ingredient['name'];
            this.name_plural = ingredient['description'];
            this.photo = ingredient['photo'];
            this.unitIngredient = ingredient['unitIngredient'];
            this.categoryIngredient = ingredient['categoryIngredient'];
            this.calories = ingredient['calories'];
            this.total_carbohydrates = ingredient['total_carbohydrates'];
            this.sugar = ingredient['sugar'];
            this.total_fat = ingredient['total_fat'];
            this.fat_calories = ingredient['fat_calories'];
            this.saturated_fat = ingredient['saturated_fat'];
            this.trans_fat = ingredient['trans_fat'];
            this.cholesterol = ingredient['cholesterol'];
            this.sodium = ingredient['sodium'];
            this.total_carbs = ingredient['total_carbs'];
            this.dietary_fiber = ingredient['dietary_fiber'];
            this.total_sugar = ingredient['total_sugar'];
            this.added_sugar = ingredient['added_sugar'];
            this.protein = ingredient['protein'];
            this.potassium = ingredient['potassium'];
            this.vitamin_a = ingredient['vitamin_a'];
            this.vitamin_c = ingredient['vitamin_c'];
            this.calcium = ingredient['calcium'];
            this.iron = ingredient['iron'];
        }
    }
}
