import { Component, ViewEncapsulation } from '@angular/core';
import { FormControl } from "@angular/forms";
import { RecipesListService } from "./recipes-list.service";
import { Animations } from "../../../../core/animations";

@Component({
    selector   : 'recipes',
    templateUrl: './recipes.component.html',
    styleUrls  : ['./recipes.component.scss'],
    //how the template and the styles should be encapsulated
    encapsulation: ViewEncapsulation.None, //to use global CSS without any encapsulation
    animations   : [Animations.slideInTop]
})
export class RecipesComponent
{
    hasSelectedRecipes: boolean;
    searchInput: FormControl;

    constructor(private recipesListService: RecipesListService)
    {
        this.searchInput = new FormControl('');
    }

    ngOnInit()
    {
        //Listener when a user selects the recipes
        this.recipesListService.onSelectedRecipesChanged
            .subscribe(selectedRecipes => {
                this.hasSelectedRecipes = selectedRecipes.length > 0;
            });

        //Listener when a user starts to search in the list of recipes
        this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
                this.recipesListService.onSearchTextChanged.next(searchText);
            });
    }
}
