import { Component, OnInit, ViewChild } from '@angular/core';
import { RecipesListService } from '../recipes-list.service';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { MdDialog, MdDialogRef } from "@angular/material";
import { FuseConfirmDialogComponent } from "../../../../../core/components/confirm-dialog/confirm-dialog.component";
import { RecipeFormDialogComponent } from "../recipe-form/recipe-form.component";
import { FormGroup } from "@angular/forms";
import { FileService } from "../../../../../core/services/file/file.service";
import { Page } from "../../page.model";

@Component({
    selector: 'recipe-list',
    templateUrl: './recipe-list.component.html',
    styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {

    size: number = 100;
    page = new Page(this.size);
    recipes = [];
    selectedRecipes = [];
    selected = [];
    dialogRef: any;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    checkboxes: {};

    constructor(private recipesListService: RecipesListService,
                private fileService: FileService,
                public dialog: MdDialog) {
        this.recipesListService.onRecipesChanged.subscribe(recipes => {
            this.recipes = recipes;

            if (this.recipes.length > 0) {
                this.page.totalElements = this.recipes[0].totalRows;
                this.page.totalPages = this.page.totalElements / this.page.size;
            }else{
                this.page = new Page(this.size);
            }
        });


        this.recipesListService.onSelectedRecipesChanged.subscribe(selectedRecipes => {
            for ( const id in this.checkboxes )
            {
                this.checkboxes[id] = selectedRecipes.includes(id);
            }
            this.selected = selectedRecipes;

            if(this.selected.length == this.recipes.length){
                this.selectedRecipes = [];
                this.recipes.forEach((recipe) => {
                    this.selectedRecipes.push(recipe);
                });
            }else{
                if(this.selected.length == 0){
                    this.selectedRecipes = [];
                }
            }
        });

    }

    ngOnInit() {
        //update the object Page
        this.recipesListService.onPageChanged.next(this.page);
    }

    /**
     * Populate the table with new data based on the page number
     *
     * @param page The page to select
     */
    setPage(page) {
        this.page.pageNumber = page.offset;

        //update the object Page
        this.recipesListService.onPageChanged.next(this.page);
    }


    /**
     * Delete the Recipe from the Backand
     *
     * @param recipe
     */
    removeRecipe(recipe) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.removeImageRecipe(recipe.photo);
                this.recipesListService.deleteRecipe(recipe.id);
            }
            this.confirmDialogRef = null;
        });
    }

    /**
     * Delete a photo of Recipe
     *
     * @param photo
     */
    removeImageRecipe(photo) {
        let indexLast = photo.lastIndexOf('/');
        let fileName = photo.slice(indexLast + 1, photo.length);
        this.fileService.remove('recipes', fileName);
    }

    /**
     * Open the dialog for editing the Recipe
     *
     * @param recipe
     */
    editRecipe(recipe) {

        this.dialogRef = this.dialog.open(RecipeFormDialogComponent, {
            disableClose: true,
            panelClass: 'recipe-form-dialog',
            data: {
                recipe: recipe,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                } else {
                    if (response[0] == "delete") {
                        this.removeImageRecipe(recipe.photo);
                        this.recipesListService.deleteRecipe(recipe.id);
                    }
                }
            });
    }

    /**
     * Create a new Recipe
     */
    newRecipe() {
        this.dialogRef = this.dialog.open(RecipeFormDialogComponent, {
            disableClose: true,
            panelClass: 'recipe-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {

                if (!response) {
                    return;
                }

                this.recipesListService.updateRecipe();
            });
    }

    /**
     * Trigger a selected / unselected recipe
     *
     * @param recipe
     */
    onSelectedChange(recipe)
    {
        this.recipesListService.toggleSelectedRecipe(recipe.id);
    }

    /**
     * Toggle the selected Recipe
     */
    onSelectedAllChange(){
        if(this.selectedRecipes.length == this.recipes.length) {
            this.selectedRecipes = [];
            this.deselectAll();
        }else{
            this.selectedRecipes = [];
            this.recipes.forEach((recipe) => {
                this.selectedRecipes.push(recipe);
            });

            this.selectAll();
        }
    }

    /**
     * Select all recipes
     */
    selectAll() {
        this.recipesListService.selectRecipes();
    }

    /**
     * Deselect all recipe
     */
    deselectAll() {
        this.recipesListService.deselectRecipes();
    }
}
