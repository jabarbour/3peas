import { Component, OnInit } from '@angular/core';
import { RecipesListService } from '../recipes-list.service';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../../core/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls: ['./selected-bar.component.scss']
})
export class SelectedBarComponent implements OnInit {
    selectedRecipes: string[];
    hasSelectedRecipes: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;

    constructor(private recipesListService: RecipesListService,
                public dialog: MdDialog) {
        this.recipesListService.onSelectedRecipesChanged
            .subscribe(selectedRecipes => {
                this.selectedRecipes = selectedRecipes;
                setTimeout(() => {
                    this.hasSelectedRecipes = selectedRecipes.length > 0;
                    this.isIndeterminate = (selectedRecipes.length !== this.recipesListService.recipes.length && selectedRecipes.length > 0);
                }, 0);
            });

    }

    ngOnInit() {
    }

    selectAll() {
        this.recipesListService.selectRecipes();
    }

    deselectAll() {
        this.recipesListService.deselectRecipes();
    }

    deleteSelectedRecipes() {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected recipes?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.recipesListService.deleteSelectedRecipes();
            }
            this.confirmDialogRef = null;
        });
    }

}
