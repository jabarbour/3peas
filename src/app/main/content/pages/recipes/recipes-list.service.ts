import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { BackandService } from "@backand/angular2-sdk";
import { RecipesService } from "../../../../core/services/recipes/recipes.service";
import { FileService } from "../../../../core/services/file/file.service";
import { Page } from "../page.model";

@Injectable()
export class RecipesListService implements Resolve<any> {
    onRecipesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onSelectedRecipesChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSearchTextChanged: Subject<any> = new Subject();
    onPageChanged: Subject<any> = new Subject();
    recipes = [];
    selectedRecipes: string[] = [];
    searchText: string = "";
    page = new Page();

    constructor(private backand: BackandService,
                private fileService: FileService,
                private recipesService: RecipesService) {
    }

    /**
     * The Recipes App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([])
                .then(
                    () => {

                        this.onPageChanged.subscribe(page => {
                            this.page = page;
                            this.getRecipes(this.page, this.searchText);
                        });

                        this.onSearchTextChanged.subscribe(searchText => {
                            this.searchText = searchText;
                            this.getRecipes(this.page, searchText);
                        });

                        resolve();
                    },
                    reject
                );
        });
    }

    /**
     * Populate the table with new data based on the page number
     *
     * @param page
     * @param filter
     * @returns {Promise}
     */
    getRecipes(page, filter?): Promise<any> {
        return new Promise((resolve, reject) => {
            page.pageNumber = typeof filter == 'undefined' || filter == "" ? page.pageNumber : 0;

            this.recipesService.getList(filter, page.size, page.pageNumber)
                .then((result) => {
                    this.recipes = result['data'];
                    this.onRecipesChanged.next(this.recipes);
                    resolve(this.recipes);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    /**
     * Toggle selected recipe by id
     *
     * @param id
     */
    toggleSelectedRecipe(id) {
        // First, check if we already have that todo as selected...
        if (this.selectedRecipes.length > 0) {
            const index = this.selectedRecipes.indexOf(id);

            if (index !== -1) {
                this.selectedRecipes.splice(index, 1);

                // Trigger the next event
                this.onSelectedRecipesChanged.next(this.selectedRecipes);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedRecipes.push(id);

        // Trigger the next event
        this.onSelectedRecipesChanged.next(this.selectedRecipes);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll() {
        if (this.selectedRecipes.length > 0) {
            this.deselectRecipes();
        }
        else {
            this.selectRecipes();
        }
    }

    /**
     * Select a recipe
     *
     * @param filterParameter
     * @param filterValue
     */
    selectRecipes(filterParameter?, filterValue?) {
        this.selectedRecipes = [];

        // If there is no filter, select all todos
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedRecipes = [];
            this.recipes.map(recipe => {
                this.selectedRecipes.push(recipe.id);
            });
        }
        else {
            /* this.selectedRecipes.push(...
             this.recipes.filter(todo => {
             return todo[filterParameter] === filterValue;
             })
             );*/
        }

        // Trigger the next event
        this.onSelectedRecipesChanged.next(this.selectedRecipes);
    }

    updateRecipe() {
        this.getRecipes(this.page, this.searchText);
    }


    /**
     * Deselect all the recipes
     */
    deselectRecipes() {
        this.selectedRecipes = [];

        // Trigger the next event
        this.onSelectedRecipesChanged.next(this.selectedRecipes);
    }

    /**
     * Delete the Recipe from the Backand
     *
     * @param recipeId
     */
    deleteRecipe(recipeId) {
        this.recipesService.remove(recipeId)
            .then((result) => {
                this.getRecipes(this.page, this.searchText);
            })
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * Delete a photo of Recipe
     *
     * @param photo
     */
    removeImageRecipe(photo) {
        let indexLast = photo.lastIndexOf('/');
        let fileName = photo.slice(indexLast + 1, photo.length);
        this.fileService.remove('recipes', fileName);
    }

    /**
     * Delete the selected Recipes
     */
    deleteSelectedRecipes() {
        for (const recipeId of this.selectedRecipes) {
            const recipe = this.recipes.find(_recipe => {
                return _recipe.id === recipeId;
            });

            this.deleteRecipe(recipeId);
            this.removeImageRecipe(recipe.photo);

            const recipeIndex = this.recipes.indexOf(recipe);
            this.recipes.splice(recipeIndex, 1);
        }
        this.onRecipesChanged.next(this.recipes);
        this.deselectRecipes();
    }

}
