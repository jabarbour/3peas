/**
 * An object used to get recipe information from the Backand
 */
export class Recipe {

    //The main info about the Recipe
    id: number;
    name: string = '';
    description: string = '';
    photo: string = '';
    difficulty: number;
    rating: number;
    prep_time: number;
    cook_time: number;
    servings: number = 4;
    ingredient_count: number;
    instructions: string = '';

    //The tags for the Recipe
    tags: Array<any> = [];
    //The nutrition Recipe
    nutrition: any = {};
    //The ingredients of the Recipe
    ingredients: Array<any> = [];


    constructor(recipe?:any){
        if(recipe){
            this.id = recipe['id'];
            this.name = recipe['name'];
            this.description = recipe['description'];
            this.photo = recipe['photo'];
            this.difficulty = recipe['difficulty'];
            this.rating = recipe['rating'];
            this.prep_time = recipe['prep_time'];
            this.cook_time = recipe['cook_time'];
            this.servings = recipe['servings'];
            this.ingredient_count = recipe['ingredient_count'];
            this.instructions = recipe['instructions'];
        }
    }

    setTags(tags){
        this.tags = tags;
    }

    setNutrition(nutrition){
        this.nutrition = nutrition;
    }

    setIngredients(ingredients){
        this.ingredients = ingredients;
    }

}