import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { UnitsService } from "../../../../../../core/services/units/units.service";


@Component({
    selector: 'ingredient-form',
    templateUrl: './ingredient-form.component.html',
    styleUrls: ['./ingredient-form.component.scss']
})
export class IngredientFormComponent implements OnInit{
    @Input() form: FormGroup; // This component is passed a FormGroup from the base component template
    filteredIngredients: Observable<string[]>;
    searchIngredient: FormControl = new FormControl('');
    @Input() ingredients = [];
    units = {
        us: [],
        eu: []
    };


    constructor(private unitsService: UnitsService) {
    }

    ngOnInit() {
        this.units = this.unitsService.getListUnits();
        this.filteredIngredients = this.searchIngredient
            .valueChanges
            .startWith(null)
            .map(val => val ? this.filter(val) : this.ingredients.slice());

        let dataForm = this.form.value;
        if(this.ingredients.length > 0) {
            let ingredient = this.ingredients.filter(ingredient => ingredient.id == dataForm.ingredientId)[0];
            
            if(ingredient)
                this.searchIngredient = new FormControl(ingredient['name_singular']);
        }
    }

    /**
     * Search for Ingredients
     *
     * @param val
     * @returns {any[]}
     */
    filter(val: string): string[] {
        return this.ingredients.filter(ingredient =>
        ingredient.name_singular.toLowerCase().indexOf(val.toLowerCase()) === 0);
    }

    /**
     * Set a value of IngredientId
     *
     * @param event
     * @param ingredientId
     */
    onSelectIngredient(event, ingredientId){
        if(event.isUserInput) {
            this.form.controls['ingredientId'].setValue(ingredientId);
        }
    }

    /**
     * Clear a value of IngredientId
     */
    clearValueIngredientId(){
        this.form.controls['ingredientId'].setValue('');
    }
}
