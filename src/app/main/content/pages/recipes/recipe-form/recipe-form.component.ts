import { Component, Inject, OnInit, ViewEncapsulation, ViewChild, Input } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import 'rxjs/Rx';
import { TagsService } from "../../../../../core/services/tags/tags.service";
import { Observable } from "rxjs";
import { ArrayType } from "@angular/compiler/src/output/output_ast";
import { IngredientsService } from "../../../../../core/services/ingredients/ingredients.service";
import { FileService } from "../../../../../core/services/file/file.service";
import { RecipesService } from "../../../../../core/services/recipes/recipes.service";
import { RecipeNutritionsService } from "../../../../../core/services/recipe-nutritions/recipe-nutritions.service";
import { RecipeIngredientsService } from "../../../../../core/services/recipe-ingredients/recipe-ingredients.service";
import { Recipe } from "./recipe/recipe.model";
import { FuseConfirmDialogComponent } from "../../../../../core/components/confirm-dialog/confirm-dialog.component";


@Component({
    selector: 'recipe-form-dialog',
    templateUrl: './recipe-form.component.html',
    styleUrls: ['./recipe-form.component.scss'],
    //how the template and the styles should be encapsulated
    encapsulation: ViewEncapsulation.None
})

export class RecipeFormDialogComponent implements OnInit {
    @ViewChild('fileInput') fileInput;
    event: CalendarEvent;
    dialogTitle: string;
    recipeForm: FormGroup;
    action: string;
    recipe: any = {};
    nutrition = {};
    recipeIngredients = [];
    uploadFile: any = {};
    tags = [];
    ingredients = [];
    removedRecipeIngredients = [];
    confirmDialogRef: MdDialogRef<FuseConfirmDialogComponent>;


    constructor(public dialogRef: MdDialogRef<RecipeFormDialogComponent>,
                private tagsService: TagsService,
                public dialog: MdDialog,
                private ingredientsService: IngredientsService,
                @Inject(MD_DIALOG_DATA) private data: any,
                private fileService: FileService,
                private recipeNutritionsService: RecipeNutritionsService,
                private recipeIngredientsService: RecipeIngredientsService,
                private recipesService: RecipesService,
                private formBuilder: FormBuilder) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Recipe';

            if (data.recipe.id) {
                this.fillRecipeModel(data.recipe);
            }
        }
        else {
            this.dialogTitle = 'New Recipe';
            this.recipe = new Recipe();
            this.initRecipeForm();
            this.addRecipeIngredientForm();
        }
    }

    ngOnInit() {
        //Get the full list
        this.getTags();
        this.getListIngredients();
    }


    /**
     * Fill in the model Recipe
     *
     * @param recipe
     */
    fillRecipeModel(recipe) {
        this.recipe = new Recipe(recipe);

        let promises = [];
        promises.push(this.tagsService.getListTagsByRecipeId(this.recipe.id)
            .then(
                (result) => {
                    let tagIds = result['data'].map((item) => Number(item.tagId));
                    this.recipe.setTags(tagIds);
                }
            ));

        promises.push(this.recipeNutritionsService.getNutritionsByRecipeId(this.recipe.id)
            .then(
                (result) => {
                    this.recipe.setNutrition(result['data'][0]);
                }
            ));

        promises.push(this.recipeIngredientsService.getListIngredientsByRecipeId(this.recipe.id)
            .then(
                (result) => {
                    this.recipe.setIngredients(result['data']);
                }
            ));

        //Execute the promises
        Promise.all(promises)
            .then(
                () => {
                    this.initRecipeForm();
                }
            );
    }


    /**
     * Add a new form RecipeIngredient
     */
    addRecipeIngredientForm(): void {
        const arrayControl = <FormArray>this.recipeForm.controls['newRecipeIngredients'];

        let newGroup = this.formBuilder.group({
            ingredientId: ['', [Validators.required]],
            unit_us: ['', [Validators.required]],
            quantity_us: ['', [Validators.required]],
            unit_eu: ['', [Validators.required]],
            quantity_eu: ['', [Validators.required]],
            notes: new FormControl('')
        });

        arrayControl.push(newGroup);
    }

    /**
     * Delete the RecipeIngredient form
     * @param index
     */
    removeRecipeIngredientForm(index: number, typeRecipeIngredient: string, recipeIngredientId?): void {

        if (typeRecipeIngredient == 'edit') {
            const arrayControl = <FormArray>this.recipeForm.controls['recipeIngredients'];
            arrayControl.removeAt(index);
            this.removedRecipeIngredients.push(recipeIngredientId);
        }else{
            const arrayControl = <FormArray>this.recipeForm.controls['newRecipeIngredients'];
            arrayControl.removeAt(index);
        }
    }

    /**
     * Initial the form Recipe
     */
    initRecipeForm() {

        let newForm = this.formBuilder.group({
            id: [this.recipe['id'] || ''],
            name: [this.recipe['name'] || ''],
            description: [this.recipe['description'] || ''],
            photo: [this.recipe['photo'] || ''],
            difficulty: [this.recipe['difficulty']],
            rating: [this.recipe['rating']],
            prep_time: [this.recipe['prep_time']],
            cook_time: [this.recipe['cook_time']],
            servings: [this.recipe['servings'] || 4],
            ingredient_count: [this.recipe['ingredient_count']],
            instructions: [this.recipe['instructions'] || ''],
            tags: [this.recipe.tags || ''],
            nutritions: new FormGroup({
                id: new FormControl(this.recipe.nutrition['id'] || ''),
                calories: new FormControl(this.recipe.nutrition['calories'] || ''),
                calories_from_fat: new FormControl(this.recipe.nutrition['calories_from_fat'] || ''),
                total_fat: new FormControl(this.recipe.nutrition['total_fat'] || ''),
                saturated_fat: new FormControl(this.recipe.nutrition['saturated_fat'] || ''),
                trans_fat: new FormControl(this.recipe.nutrition['trans_fat'] || ''),
                cholesterol: new FormControl(this.recipe.nutrition['cholesterol'] || ''),
                sodium: new FormControl(this.recipe.nutrition['sodium'] || ''),
                potassium: new FormControl(this.recipe.nutrition['potassium'] || ''),
                total_carbohydrates: new FormControl(this.recipe.nutrition['total_carbohydrates'] || ''),
                dietary_fiber: new FormControl(this.recipe.nutrition['dietary_fiber'] || ''),
                sugar: new FormControl(this.recipe.nutrition['sugar'] || ''),
                protein: new FormControl(this.recipe.nutrition['protein'] || ''),
                vitamin_a: new FormControl(this.recipe.nutrition['vitamin_a'] || ''),
                vitamin_c: new FormControl(this.recipe.nutrition['vitamin_c'] || ''),
                calcium: new FormControl(this.recipe.nutrition['calcium'] || ''),
                iron: new FormControl(this.recipe.nutrition['iron'] || ''),
                servings: new FormControl(this.recipe.nutrition['servings'] || 1) // default value
            }),
            recipeIngredients: this.formBuilder.array([]), //this.initialRecipeIngredients(this.recipeIngredients),
            newRecipeIngredients: this.formBuilder.array([])
        });

        const arrayControl = <FormArray>newForm.controls['recipeIngredients'];
        if (this.recipe.ingredients) {
            this.recipe.ingredients.forEach(recipeIngredient => {
                let newGroup = this.formBuilder.group({
                    id: [recipeIngredient['id']],
                    recipeId: [recipeIngredient['recipeId']],
                    ingredientId: [recipeIngredient['ingredientId'], [Validators.required]],
                    unit_us: [Number(recipeIngredient['unit_us']), [Validators.required]],
                    quantity_us: [recipeIngredient['quantity_us'], [Validators.required]],
                    unit_eu: [Number(recipeIngredient['unit_eu']), [Validators.required]],
                    quantity_eu: [recipeIngredient['quantity_eu'], [Validators.required]],
                    notes: [recipeIngredient['notes']]
                });
                arrayControl.push(newGroup);
            });
        }

        this.recipeForm = newForm;
    }


    /**
     * Change a file
     *
     * @param $event
     */
    onFileChange($event) {
        if ($event.target.files.length > 0) {
            let file = $event.target.files[0];
            let myReader: FileReader = new FileReader();

            myReader.onloadend = (e) => {
                this.recipeForm.controls['photo'].setValue(file);
                this.uploadFile = {
                    fileName: file.name,
                    base64: myReader.result
                };
            };
            myReader.readAsDataURL(file);
        } else {
            this.recipeForm.controls['photo'].setValue('');
            this.uploadFile = {};
        }
    }

    /**
     * Get the list of the Tags
     */
    getTags() {
        this.tagsService.getListTags()
            .then(
                (result) => {
                    this.tags = result['data'];
                }
            );
    }


    /**
     * Get the list of all ingredients
     */
    getListIngredients() {
        this.ingredientsService.getList()
            .then(
                (result) => {
                    this.ingredients = result['data'];
                }
            )
    }


    /**
     * Save a new recipe to the Backand
     */
    createRecipe() {

        let dataForm = this.recipeForm.value;
        let fileName = this.uploadFile.fileName.replace(/ /gi, '_');

        //Upload a new image
        this.fileService.upload("recipes", fileName, this.uploadFile.base64)
            .then(
                (fileUrl) => {
                    if (fileUrl) {
                        dataForm['photo'] = fileUrl;
                        dataForm['ingredient_count'] = dataForm.newRecipeIngredients.length;

                        this.recipesService.create(dataForm)
                            .then(
                                (result) => {
                                    //Create a new recipe
                                    if (this.action != 'edit') {
                                        let recipeId = result['data'].__metadata.id;
                                        dataForm.nutritions['recipeId'] = recipeId;

                                        let promises = [];

                                        //Create a new ingredient(s) for the recipe
                                        promises.push(this.createNutritionsRecipe(dataForm.nutritions));

                                        //Create a new ingredient(s) for the recipe
                                        promises.push(dataForm.newRecipeIngredients.forEach((recipeIngredient) => {
                                            recipeIngredient['recipeId'] = recipeId;
                                            this.createIngredientRecipe(recipeIngredient);
                                        }));

                                        //Create a new tag Recipe
                                        dataForm.tags.forEach((tag) => {
                                            let tagRecipe = {
                                                recipeId: recipeId,
                                                tagId: tag
                                            };

                                            promises.push(this.createTagRecipe(tagRecipe));
                                        });

                                        //Execute the promises
                                        Promise.all(promises)
                                            .then(
                                                () => {
                                                    this.dialogRef.close('save');
                                                }
                                            );
                                    }
                                })
                            .catch(error => {
                                console.log(error);
                            });
                    }
                }
            )
            .catch(error => {
                console.log(error);
            });
    }


    /**
     * Update the recipe to the Backand
     */
    updateRecipe() {

        let dataForm = this.recipeForm.value;
        let promises = [];

        //Upload a new image
        if (typeof this.uploadFile.fileName != 'undefined') {
            let fileName = this.uploadFile.fileName.replace(/ /gi, '_');

            promises.push(this.removeImageRecipe(this.recipe.photo));
            promises.push(this.fileService.upload("recipes", fileName, this.uploadFile.base64)
                .then(
                    (fileUrl) => {
                        if (fileUrl) {
                            dataForm['photo'] = fileUrl;
                            dataForm['ingredient_count'] = dataForm.newRecipeIngredients.length;
                        }
                    }
                ));
        }

        Promise.all(promises)
            .then(
                () => {
                    console.log(dataForm);

                    dataForm['ingredient_count'] = dataForm.newRecipeIngredients.length +  dataForm.recipeIngredients.length;

                    this.recipesService.update(dataForm.id, dataForm)
                        .then(
                            (result) => {

                                promises = [];

                                //Update a new ingredient(s) for the recipe
                                promises.push(this.updateNutritionsRecipe(dataForm.nutritions));

                                //Create a new ingredient(s) for the recipe
                                if(dataForm.newRecipeIngredients.length > 0) {
                                    promises.push(dataForm.newRecipeIngredients.forEach((recipeIngredient) => {
                                        recipeIngredient['recipeId'] = dataForm.id;
                                        this.createIngredientRecipe(recipeIngredient);
                                    }));
                                }

                                //Update the ingredient(s) for the recipe
                                if(dataForm.recipeIngredients.length > 0) {
                                    promises.push(dataForm.recipeIngredients.forEach((recipeIngredient) => {
                                        this.updateIngredientRecipe(recipeIngredient);
                                    }));
                                }

                                //Delete the ingredient(s) of the recipe
                                if(this.removedRecipeIngredients.length > 0) {
                                    promises.push(this.removedRecipeIngredients.forEach((recipeIngredient) => {
                                        this.removeIngredientRecipe(recipeIngredient);
                                    }));
                                }


                                let newTagIds = dataForm.tags.filter(item => this.recipe.tags.indexOf(item) < 0);
                                let removedTagIds = this.recipe.tags.filter(item => dataForm.tags.indexOf(item) < 0);

                                //Create a new tag Recipe
                                if(newTagIds.length > 0) {
                                    newTagIds.forEach((tag) => {
                                        let tagRecipe = {
                                            recipeId: dataForm.id,
                                            tagId: tag
                                        };

                                        promises.push(this.createTagRecipe(tagRecipe));
                                    });
                                }

                                //Remove the tag Recipe
                                if(removedTagIds.length > 0) {
                                    promises.push(this.removeTagRecipe(dataForm.id, removedTagIds));
                                }


                                //Execute the promises
                                Promise.all(promises)
                                    .then(
                                        () => {
                                            this.dialogRef.close('save');
                                        }
                                    );
                            })
                        .catch(error => {
                            console.log(error);
                        });
                }
            )
    }

    removeRecipe(){
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.dialogRef.close(['delete']);
            }
            this.confirmDialogRef = null;
        });
    }


    /**
     * Delete a photo of Recipe
     *
     * @param photo
     */
    removeImageRecipe(photo) {
        let indexLast = photo.lastIndexOf('/');
        let fileName = photo.slice(indexLast + 1, photo.length);
        this.fileService.remove('recipes', fileName);
    }


    /**
     * Create a new tag Recipe
     *
     * @param tagRecipe
     */
    createTagRecipe(tagRecipe) {
        this.tagsService.create(tagRecipe)
            .then(
                (result) => {
                    //success
                }
            )
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * Create a new tag Recipe
     *
     * @param recipeId
     * @param tagIds
     */
    removeTagRecipe(recipeId, tagIds) {
        this.tagsService.remove(recipeId, tagIds.toString())
            .then(
                (result) => {
                    //success
                }
            )
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * Create a new ingredient Recipe
     *
     * @param recipeIngredient
     */
    createIngredientRecipe(recipeIngredient) {
        this.recipeIngredientsService.create(recipeIngredient)
            .then(
                (result) => {
                    //success
                }
            )
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * Update the ingredient Recipe
     *
     * @param recipeIngredient
     */
    updateIngredientRecipe(recipeIngredient) {
        this.recipeIngredientsService.update(recipeIngredient.id, recipeIngredient)
            .then(
                (result) => {
                    //success
                }
            )
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * Delete the ingredient Recipe
     *
     * @param recipeIngredient
     */
    removeIngredientRecipe(recipeIngredient) {
        this.recipeIngredientsService.remove(recipeIngredient)
            .then(
                (result) => {
                    //success
                }
            )
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * Create a new nutritions Recipe
     *
     * @param nutritions
     */
    createNutritionsRecipe(nutritions) {
        this.recipeNutritionsService.create(nutritions)
            .then(
                (result) => {
                    //success
                }
            )
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * Update the nutritions Recipe
     *
     * @param nutritions
     */
    updateNutritionsRecipe(nutritions) {
        this.recipeNutritionsService.update(nutritions.id, nutritions)
            .then(
                (result) => {
                    //success
                }
            )
            .catch(error => {
                console.log(error);
            });
    }
}