import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RecipesComponent } from './recipes.component';
import { RecipesListService } from "./recipes-list.service";
import { RecipeListComponent } from "./recipe-list/recipe-list.component";
import { RecipeFormDialogComponent } from "./recipe-form/recipe-form.component";
import { SelectedBarComponent } from "./selected-bar/selected-bar.component";
import { FileService } from "../../../../core/services/file/file.service";
import { TagsService } from "../../../../core/services/tags/tags.service";
import { UnitsService } from "../../../../core/services/units/units.service";
import { IngredientsService } from "../../../../core/services/ingredients/ingredients.service";
import { IngredientFormComponent } from "./recipe-form/ingredient-form/ingredient-form.component";
import { RecipesService } from "../../../../core/services/recipes/recipes.service";
import { RecipeNutritionsService } from "../../../../core/services/recipe-nutritions/recipe-nutritions.service";
import { RecipeIngredientsService } from "../../../../core/services/recipe-ingredients/recipe-ingredients.service";

const routes = [
    {
        path     : '**',
        component: RecipesComponent,
        resolve  : {
            recipes: RecipesListService
        }
    }
];

@NgModule({
    //every component must be declared in some NgModule
    //every component can belong to only one NgModule
    declarations: [
        RecipesComponent,
        RecipeListComponent,
        SelectedBarComponent,
        RecipeFormDialogComponent,
        IngredientFormComponent,
        SelectedBarComponent
    ],
    // import other modules as dependencies
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    //provider is to be injected,
    // create and maintain a singleton instance of that class and pass it in the injection
    providers   : [
        RecipesListService,
        FileService,
        TagsService,
        UnitsService,
        IngredientsService,
        RecipesService,
        RecipeNutritionsService,
        RecipeIngredientsService
    ],
    //it registers components for offline compilation
    // so that they can be used with ViewContainerRef.createComponent()
    entryComponents: [RecipeFormDialogComponent]
})

export class RecipesModule
{
}
