import { NgModule } from '@angular/core';
import { LoginModule } from './login/login.module';
import { DashboardModule } from "./dashboard/dashboard.module";

@NgModule({
    imports: [
        // Auth
        LoginModule,

        // Dashboard
        DashboardModule
    ]
})
export class PagesModule
{
}
