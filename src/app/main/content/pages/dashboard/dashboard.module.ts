import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../core/modules/shared.module';
import { DashboardComponent } from './dashboard.component';

const routes = [
    {
        path     : 'dashboard',
        component: DashboardComponent
    }
];

@NgModule({
    declarations: [
        DashboardComponent
    ],
    // import other modules as dependencies
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    //the list of public components
    exports     : [
        DashboardComponent
    ]
})

export class DashboardModule
{
}
