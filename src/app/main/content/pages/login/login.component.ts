import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../../../core/services/config.service';
import { BackandService } from "@backand/angular2-sdk";
import { Router } from "@angular/router";
import { MessageService } from "../../../../core/services/message.service";
import 'rxjs/add/operator/toPromise';


@Component({
    selector: 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class FuseLoginComponent implements OnInit {
    loginForm: FormGroup;
    loginFormErrors: any;

    constructor(private fuseConfig: FuseConfigService,
                private backand: BackandService,
                private message: MessageService,
                private router: Router,
                private formBuilder: FormBuilder) {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.loginFormErrors = {
            email: {},
            password: {}
        };
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    /**
     * Check user's data from the Login form
     */
    onLoginFormValuesChanged() {
        for (const field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }

    /**
     * User log in yo the account
     */
    login() {
        let data = this.loginForm.value;

        this.loginToBackand(data.email, data.password)
            .then(
                (response: any) => {
                    //get userId
                    return response.data.userId;
                }
            )
            .then(
                (userId) => {
                    if(userId){
                        this.getUser(userId)
                            .then(
                                (response: any) => {
                                    if (response.data.is_admin === true) {
                                        //show a message
                                        this.message.show('Success', "You are logged in to your account.", "blue-500-fg");

                                        //close a message
                                        setTimeout(() => {
                                            this.message.close();
                                        }, 1200);

                                        setTimeout(() => {
                                            this.router.navigate(['/']);
                                        }, 1500);

                                    } else {
                                        this.message.show('Error', 'Sorry, but you have no an access.', "warn-500-fg");
                                    }
                                }
                            );
                    }else{
                        localStorage.clear();

                        //show a message
                        this.message.show('Error on the Backand', "Need to clear the registered users.", "warn-500-fg");

                        //close a message
                        setTimeout(() => {
                            this.message.close();
                        }, 1200);
                    }
                }
            )
            .catch(
                (error: any) => {
                    let errorMessage = error.data.error_description;

                    //show a message
                    this.message.show('Error', errorMessage, "warn-500-fg");
                });
    }

    /**
     * User login to the Backand
     *
     * @param email
     * @param password
     */

    loginToBackand(email, password): Promise<any> {
        return new Promise((resolve, reject) => {

            this.backand.signin(email, password)
                .then(
                    (success: any) => {
                        resolve(success);
                    }
                )
                .catch(
                    (error: any) => {
                        reject(error);
                    });
        });
    }

    /**
     * Get user information
     *
     * @param userId
     */
    getUser(userId): Promise<any> {
        return new Promise((resolve, reject) => {
            this.backand.object.getOne('users', userId)
                .then(
                    (success: any) => {
                        localStorage.setItem('userData', JSON.stringify(success.data));
                        resolve(success);
                    }
                )
                .catch(
                    (error: any) => {
                        reject(error);
                    });
        });
    }
}
