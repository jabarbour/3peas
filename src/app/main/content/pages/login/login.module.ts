import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';
import { FuseLoginComponent } from './login.component';

const routes = [
    {
        path     : 'login',
        component: FuseLoginComponent
    }
];

@NgModule({
    declarations: [
        FuseLoginComponent
    ],
    // import other modules as dependencies
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    //the list of public components
    exports     : [
        FuseLoginComponent
    ]
})

export class LoginModule
{

}
