export class FuseNavigation
{
    public items: any[];

    constructor()
    {
        this.items = [
            // {
            //     'title': 'APPS',
            //     'type' : 'subheader'
            // },
            {
                'title': 'Dashboard',
                'type' : 'nav-item',
                'icon' : 'dashboard',
                'url'  : '/dashboard'
            },
            {
                'title': 'Recipes',
                'type' : 'nav-item',
                'icon' : 'folder',
                'url'  : '/recipes'
            },
            {
                'title': 'Ingredients',
                'type' : 'nav-item',
                'icon' : 'folder',
                'url'  : '/ingredients'
            },
            {
                'title': 'Products',
                'type' : 'nav-item',
                'icon' : 'folder',
                'url'  : '/products'
            }
        ];
    }
}
