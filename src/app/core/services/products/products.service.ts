import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";


@Injectable()
export class ProductsService {

    constructor(private backand: BackandService) {
    }

    /**
     * Get the list of Products with searchWord
     *
     * @returns {Promise<T>}
     */
    getList(search?) {
        return new Promise((resolve, reject) => {
            this.backand.query.post("getListProducts", {
                "search": search || ''
            })
                .then(products => {
                    resolve(products);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    /**
     * Get the list of Products with searchWord with pagination
     *
     * @param search
     * @param pageSize
     * @param pageNumber
     * @returns {Promise}
     */
    getListWithPagination(search, pageSize, pageNumber) {
        return new Promise((resolve, reject) => {
            this.backand.query.post("getListProductsWithSearchForWebAdmin", {
                "search": search || "",
                "pageSize": pageSize,
                "pageNumber": pageSize * pageNumber
            })
                .then(products => {
                    resolve(products);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Create a new Product
     *
     * @param data
     * @returns {Promise}
     */
    create(data) {
        return new Promise((resolve, reject) => {
            this.backand.object.create('products', data)
                .then(result => {
                    console.log('object created');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }
}
