import { Injectable } from '@angular/core';
import { StatusDialogComponent } from "../components/status-dialog/status-dialog.component";
import { MdDialog, MdDialogRef } from "@angular/material";

@Injectable()
export class MessageService {

  statusDialogRef: MdDialogRef<StatusDialogComponent>;

  constructor(private dialog: MdDialog) { }

  /**
   * Show the message of status of the request
   *
   * @param status
   * @param message
   * @param colorClass
   */
  show(status, message, colorClass){
    this.statusDialogRef = this.dialog.open(StatusDialogComponent, {
      disableClose: false
    });

    this.statusDialogRef.componentInstance.message = message;
    this.statusDialogRef.componentInstance.status = status;
    this.statusDialogRef.componentInstance.color = colorClass;
  }

  close(){
    this.dialog.closeAll();
  }

}
