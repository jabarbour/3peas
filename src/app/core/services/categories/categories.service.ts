import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";


@Injectable()
export class CategoriesService {

    categories = [];

    constructor(public backand: BackandService) {
        if (!JSON.parse(localStorage.getItem('categories'))) {
            this.setList();
        } else {
            this.categories = JSON.parse(localStorage.getItem('categories'));
        }
    }

    /**
     * Set list of measurements to localStorage
     */
    setList() {
        let config = {
            "pageSize": null,
            "pageNumber": 1,
        };

        this.backand.object.getList('product_categories', config)
            .then((result) => {
                this.categories = result.data;
                localStorage.setItem('categories', JSON.stringify(this.categories));
            })
            .catch((error) => {
                console.log(error);
            });
    }

    /**
     * Get list of product categories
     */
    getList() {
        if (JSON.parse(localStorage.getItem('categories'))) {
            this.categories = JSON.parse(localStorage.getItem('categories'));
        }

        return this.categories;
    }

}
