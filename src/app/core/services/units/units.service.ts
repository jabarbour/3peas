import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";


@Injectable()
export class UnitsService {

    units = {
        us: [],
        eu: []
    };

    constructor(public backand: BackandService) {
        if (!JSON.parse(localStorage.getItem('units'))) {
            this.setListUnits('us');
            this.setListUnits('eu');
        } else {
            this.units = JSON.parse(localStorage.getItem('units'));
        }
    }

    /**
     * Set list of measurements to localStorage
     *
     * @param country
     */
    setListUnits(country) {
        let objectName = '';
        switch (country) {
            case 'eu':
                objectName = 'eu_standart_units';
                break;
            case 'us':
                objectName = 'usa_standart_units';
                break;
        }

        this.backand.query.post('getListUnits', {objectName: objectName})
            .then((result) => {
                this.units[country] = result.data;
                localStorage.setItem('units', JSON.stringify(this.units));
            })
            .catch((error) => {
                console.log(error);
            });
    }

    /**
     * Get list of measurements
     */
    getListUnits() {
        if (JSON.parse(localStorage.getItem('units'))) {
            this.units = JSON.parse(localStorage.getItem('units'));
        }

        return this.units;
    }

}
