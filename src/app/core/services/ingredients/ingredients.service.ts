import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";


@Injectable()
export class IngredientsService {

    constructor(private backand: BackandService) {
    }

    /**
     * Get the list of Ingredients with searchWord
     *
     * @returns {Promise<T>}
     */
    getList(search?) {
        return new Promise((resolve, reject) => {
            this.backand.query.post("getListIngredients", {
                "search": search || ''
            })
                .then(ingredients => {
                    resolve(ingredients);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    /**
     * Get the list of Ingredients with searchWord with pagination
     *
     * @param search
     * @param pageSize
     * @param pageNumber
     * @returns {Promise}
     */
    getListWithPagination(search, pageSize, pageNumber) {
        return new Promise((resolve, reject) => {
            this.backand.query.post("getListIngredientsWithSearchForWebAdmin", {
                "search": search || "",
                "pageSize": pageSize,
                "pageNumber": pageSize * pageNumber
            })
                .then(ingredients => {
                    resolve(ingredients);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Create a new Ingredient
     *
     * @param data
     * @returns {Promise}
     */
    create(data) {
        return new Promise((resolve, reject) => {
            this.backand.object.create('ingredients', data)
                .then(result => {
                    console.log('object created');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }
}
