import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";

@Injectable()
export class TagsService {

    constructor(private backand: BackandService) {
    }

    /**
     * Get the list of Tags for the recipes
     *
     * @returns {Promise}
     */
    getListTags() {
        return new Promise((resolve, reject) => {
            this.backand.query.post("getListTags")
                .then(result => {
                    resolve(result)
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    /**
     * Create a new tag for the recipe
     *
     * @param data
     * @returns {Promise}
     */
    create(data) {
        return new Promise((resolve, reject) => {
            this.backand.object.create('recipe_tags', data)
                .then(result => {
                    console.log('object created');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    /**
     * Delete the tag for the recipe
     *
     * @returns {Promise}
     * @param recipeId
     * @param tagIds
     */
    remove(recipeId, tagIds) {
        return new Promise((resolve, reject) => {
            this.backand.query.post("removeRecipeTags", {
                recipeId: recipeId,
                tagIds: tagIds
            })
                .then(result => {
                    console.log('object delete');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }


    /**
     * Get the list of Tags by recipeId
     *
     * @param recipeId
     * @returns {Promise}
     */
    getListTagsByRecipeId(recipeId) {
        return new Promise((resolve, reject) => {
            this.backand.object.getList("recipe_tags", {
                "pageSize": null,
                "pageNumber": 1,
                "filter": [
                    {
                        "fieldName": "recipeId",
                        "operator": "in",
                        "value": recipeId
                    }
                ]
            })
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                })
        });
    }

}
