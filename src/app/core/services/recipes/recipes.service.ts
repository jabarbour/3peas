import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";

@Injectable()
export class RecipesService {

    constructor(private backand: BackandService) {
    }

    /**
     * Create a new Recipe
     *
     * @param data
     * @returns {Promise}
     */
    create(data) {
        return new Promise((resolve, reject) => {
            this.backand.object.create('recipes', data)
                .then(result => {
                    console.log('object created');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Update the Recipe
     *
     * @param recipeId
     * @param data
     * @returns {Promise}
     */
    update(recipeId, data) {
        return new Promise((resolve, reject) => {
            this.backand.object.update("recipes", recipeId, data)
                .then(result => {
                    console.log('object updated');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Delete the Recipe from the Backand
     *
     * @param recipeId
     */
    remove(recipeId) {
        return new Promise((resolve, reject) => {
            this.backand.object.remove("recipes", recipeId)
                .then(result => {
                    console.log('delete object');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Populate the table with new data based on the page number
     *
     * @param filter
     * @param pageSize
     * @param pageNumber
     * @returns {Promise}
     */
    getList(filter, pageSize, pageNumber) {
        return new Promise((resolve, reject) => {
            this.backand.query.post("getListRecipesWithSearchForWebAdmin", {
                "search": filter || "",
                "pageSize": pageSize,
                "pageNumber": pageSize * pageNumber
            })
                .then(result => {
                    console.log('get list');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }
}
