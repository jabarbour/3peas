import { TestBed, inject } from '@angular/core/testing';

import { RecipeIngredientsService } from './recipe-ingredients.service';

describe('RecipeIngredientsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipeIngredientsService]
    });
  });

  it('should be created', inject([RecipeIngredientsService], (service: RecipeIngredientsService) => {
    expect(service).toBeTruthy();
  }));
});
