import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";

@Injectable()
export class RecipeIngredientsService {

    constructor(private backand: BackandService) {
    }

    /**
     * Create a new ingredient for the recipe
     *
     * @param data
     * @returns {Promise}
     */
    create(data) {
        return new Promise((resolve, reject) => {
            this.backand.object.create('recipe_ingredients', data)
                .then(result => {
                    console.log('object created');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Update the ingredient for the recipe
     *
     * @param recipeIngredientId
     * @param data
     * @returns {Promise}
     */
    update(recipeIngredientId, data) {
        return new Promise((resolve, reject) => {
            this.backand.object.update("recipe_ingredients", recipeIngredientId, data)
                .then(result => {
                    console.log('object updated');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Remove the ingredient for the recipe
     *
     * @param recipeIngredientId
     * @returns {Promise}
     */
    remove(recipeIngredientId) {
        return new Promise((resolve, reject) => {
            this.backand.object.remove("recipe_ingredients", recipeIngredientId)
                .then(result => {
                    console.log('object remove');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Get the list of Ingredients by recipeId
     *
     * @param recipeId
     * @returns {Promise}
     */
    getListIngredientsByRecipeId(recipeId) {
        return new Promise((resolve, reject) => {
            this.backand.object.getList("recipe_ingredients", {
                "pageSize": null,
                "pageNumber": 1,
                "filter": [
                    {
                        "fieldName": "recipeId",
                        "operator": "in",
                        "value": recipeId
                    }
                ]
            })
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                })
        });
    }

}
