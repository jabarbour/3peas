import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";

@Injectable()
export class FileService {

    constructor(public backand: BackandService) {
    }

    /**
     * Upload a file to the Backand
     *
     * @param object
     * @param filename
     * @param filedata
     * @returns {Promise}
     */
    upload(object, filename, filedata) {
        return new Promise((resolve, reject) => {
            this.backand.file.upload(object, 'files', filename, filedata)
                .then(res => {
                    console.log('file uploaded. url: ' + res.data.url);
                    resolve(res.data.url);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                });
        });
    }

    /**
     * Delete a file from the Backand
     *
     * @param object
     * @param filename
     * @returns {Promise}
     */
    remove(object, filename) {
        return new Promise((resolve, reject) => {
            this.backand.file.remove(object, 'files', filename)
                .then(res => {
                    console.log('file deleted');
                    resolve(res);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                });
        });
    }

}
