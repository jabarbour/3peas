import { Injectable } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";

@Injectable()
export class RecipeNutritionsService {

    constructor(private backand: BackandService) {
    }

    /**
     * Create a new nutrition Recipe
     *
     * @param data
     * @returns {Promise}
     */
    create(data) {
        return new Promise((resolve, reject) => {
            this.backand.object.create('recipe_nutritions', data)
                .then(result => {
                    console.log('object created');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Update the nutrition Recipe
     *
     * @param recipeNutritionId
     * @param data
     * @returns {Promise}
     */
    update(recipeNutritionId, data) {
        return new Promise((resolve, reject) => {
            this.backand.object.update("recipe_nutritions", recipeNutritionId, data)
                .then(result => {
                    console.log('object updated');
                    resolve(result);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    }

    /**
     * Get the nutritions by recipeId
     *
     * @param recipeId
     * @returns {Promise}
     */
    getNutritionsByRecipeId(recipeId) {
        return new Promise((resolve, reject) => {
            this.backand.object.getList("recipe_nutritions", {
                "pageSize": null,
                "pageNumber": 1,
                "filter": [
                    {
                        "fieldName": "recipeId",
                        "operator": "in",
                        "value": recipeId
                    }
                ]
            })
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                })
        });
    }
}
