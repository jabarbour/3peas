import { TestBed, inject } from '@angular/core/testing';

import { RecipeNutritionsService } from './recipe-nutritions.service';

describe('RecipeNutritionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipeNutritionsService]
    });
  });

  it('should be created', inject([RecipeNutritionsService], (service: RecipeNutritionsService) => {
    expect(service).toBeTruthy();
  }));
});
