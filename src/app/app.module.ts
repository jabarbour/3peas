import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import 'hammerjs';
import { SharedModule } from './core/modules/shared.module';
import { AppComponent } from './app.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FuseMainModule } from './main/main.module';
import { FuseConfigService } from './core/services/config.service';
import { PagesModule } from "./main/content/pages/pages.module";
import { BackandService } from "@backand/angular2-sdk";
import { DashboardComponent } from "./main/content/pages/dashboard/dashboard.component";
import { MessageService } from "./core/services/message.service";
import { FuseLoginComponent } from "./main/content/pages/login/login.component";
import { AuthGuard } from "./guard/auth/auth.guard";

const appRoutes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'recipes',
        loadChildren: './main/content/pages/recipes/recipes.module#RecipesModule'
    },
    {
        path: 'ingredients',
        loadChildren: './main/content/pages/ingredients/ingredients.module#IngredientsModule'
    },
    {
        path: 'products',
        loadChildren: './main/content/pages/products/products.module#ProductsModule'
    },
    {
        path: 'login',
        component: FuseLoginComponent
    },
    // otherwise redirect to home
    {
        path        : '**',
        redirectTo: ''
    },
];

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports     : [
        BrowserModule,
        HttpModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes),
        SharedModule,
        PerfectScrollbarModule.forRoot(),
        FuseMainModule,
        PagesModule
    ],
    providers   : [
        FuseConfigService,
        BackandService,
        MessageService,
        AuthGuard
    ],
    //module as the entry point for an application
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
