import { Component, OnInit } from '@angular/core';
import { BackandService } from "@backand/angular2-sdk";
import { CONFIG } from "./constants/config";
import { Router } from "@angular/router";

@Component({
    selector: 'fuse-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

    constructor(private backand: BackandService, private router: Router) {
    }

    ngOnInit(): void {
        //init the Backand connect
        this.backand.init({
            appName: CONFIG.NAME_APP_BACKAND,
            signUpToken: CONFIG.SIGN_UP_TOKEN,
            runSocket: false,
        });

        this.checkUserAuth();
    }

    /**
     * Check user Authorization
     */
    checkUserAuth() {
        this.backand.user.getRefreshToken()
            .then(result => {
                console.log('check authorization');
                this.backand.user.getUserDetails(true)
                    .then(res => {
                        console.log('success');
                    })
                    .catch(err => {
                        if(err.status == 401){
                            localStorage.clear();
                            this.router.navigate(['/login']);
                        }
                        console.log(err);
                    });
            })
            .catch(error => {
                console.log(error);
            });
    }
}
