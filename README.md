# 3Peas Web Admin
3Peas is the serverless web application, which based on Angular 4 and the theme Fuse.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To install Angular projects, you�ll need to install the 1.1.3 version of the CLI. Before you do that, you�ll need a recent version of Node.js. Download the installer for Node.js 6.9.x till 6.11.x and npm 3.x.x, then proceed to install the Angular CLI:

```sh
$ npm install -g @angular/cli
```

### Installing

A step by step instructions for get project and start development

Clone the repository and execute the scripts from package.json:

```sh
$ git clone git@bitbucket.org:jabarbour/3peas.git
$ cd 3peas
$ npm install
```

Set settings for the App (the support email, the Backand credential a& etc):

```sh
$ mkdir src/app/constants
$ cp example-config.ts src/app/constants/config.ts
```
Create new folder under app for constants and put file config.ts imto it. Use the app name and key from Backand.
Update import {CONFIG} in app.component.ts to be import { CONFIG } from "app/constants/config";

File `.nvmrc` consists version of Node for current application, which is used by NVM

#### Development server

Then, to run the App in the web browser:
Run next command for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files:
```sh
$ ng serve
```

#### Code scaffolding

Run next command to generate a new component. You can also use `ng generate directive|pipe|service|class|module`:

```sh
$ ng generate component component-name
```

#### Build

Run next command to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build:
```sh
$ ng build
```

#### Running unit tests

Run next command to execute the unit tests via [Karma](https://karma-runner.github.io):
```sh
$ ng test
```

#### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/):
(Before running the tests make sure you are serving the app via `ng serve`)
```sh
$ ng e2e
```

## Built With

* [Nodejs](https://nodejs.org/) - The cross-platform JavaScript run-time environment
* [npm](http://nvm.sh/) - Node version manager
* [nvm](https://www.npmjs.com/) - The package manager for JavaScript
* [Angular](https://angular.io/) - The front-end web application framework
* [Backand](https://www.backand.com) - Serveless cloud service

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **John Barbour** - *Initial work* - [Nutritious Grocer](https://bitbucket.org/jabarbour/ionic)

## Developers

* **Anastasiia Kotliar** - *Development* - [Clockwise Software](https://clockwise.software)

